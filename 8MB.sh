#!/bin/bash
help() {
    echo "-i input      Input file"
    echo "-o output     Output file"
    echo "-b MBsize     Desired MB size of output video. Default 8"
    echo "-s            If subtitles should be encoded into the video"
    echo "-h            Show this help message"
    exit
}
noInput() {
    echo "Missing -i input"
    echo
    help
}
noOutput() {
    echo "Missing -o output"
    echo
    help
}
targetSizeKb=65536 # 8 MB default
optstring="i:o:b:sh"
while getopts $optstring arg; do
    case "$arg" in
        i)
            fileName="$OPTARG"
        ;;
        o)
            fileOutput="$OPTARG"
        ;;
        b)
            targetSizeKb="$(expr $OPTARG * 8 * 1024)"
        ;;
        s)
            subtitles=1
        ;;
        h)
            help
        ;;
    esac
done

if [ ! -n "$fileName" ]; then
    noInput
    elif [ ! -n "$fileOutput" ]; then
    noOutput
fi

duration=$(printf "%.0f" $(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$fileName"))
width=$(ffprobe -v error -select_streams v:0 -show_entries stream=width -of csv=s=x:p=0 "$fileName")
height=$(ffprobe -v error -select_streams v:0 -show_entries stream=height -of csv=s=x:p=0 "$fileName")
fileAudioBitrate=$(expr $(ffprobe -v error -select_streams a:0 -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "$fileName") / 1024)
bitrateKb=$(echo "scale=0; $targetSizeKb / $duration" | bc)
possibleAudioBitrate=$(echo "scale=0; $bitrateKb / 10" | bc)
if [ $possibleAudioBitrate -gt $fileAudioBitrate ]; then
    audioBitrateKb=$fileAudioBitrate
    elif [ $duration -gt 800 ] || [ $duration -lt 260 ]; then # -gt for remaining some video quality on super low bitrates and -lt for keeping as much audio quality as possible
    audioBitrateKb=$possibleAudioBitrate
else # 40Kb/s is low bitrate / good quality compromise
    audioBitrateKb=40
fi

bitrateKb=$(echo "scale=0; $bitrateKb - $audioBitrateKb" | bc)
newWidth=$width
if [ $bitrateKb -lt 24 ]; then # WHY
    newWidth=80
    tileColumns=0
    threads=2
    elif [ $bitrateKb -lt 48 ]; then # Overkill
    newWidth=160
    tileColumns=0
    threads=2
    elif [ $bitrateKb -lt 128 ]; then # Some quality
    newWidth=320
    tileColumns=0
    threads=2
    elif [ $bitrateKb -lt 192 ]; then # Decent
    newWidth=640
    tileColumns=1
    threads=4
    elif [ $bitrateKb -lt 1024 ]; then # Nice
    newWidth=1280
    tileColumns=2
    threads=8
    elif [ $bitrateKb -lt 4096 ]; then
    newWidth=1920
    tileColumns=2
    threads=8
else
    tileColumns=3
    threads=16
fi
if [ "$newWidth" -lt "$width" ]; then
    width=$newWidth
fi

if [ "$audioBitrateKb" -eq "$fileAudioBitrate" ]; then
    audioOpts="-acodec copy"
else
    audioOpts="-c:a libopus \
    -b:a ${audioBitrateKb}K"
fi

if [ -n "$subtitles" ]; then
    subtitleOpts="-vf subtitles=$fileName,scale=${width}:-2:flags=lanczos"
else
    subtitleOpts="-vf scale=${width}:-2:flags=lanczos"
fi
fileName="${fileName// /\\ }"
fileOutput="${fileOutput// /\\ }"
pass1="ffmpeg \
-i "$fileName" \
-c:v libvpx-vp9 \
-b:v ${bitrateKb}K \
-crf 30 \
-g 120 \
-tile-columns $tileColumns \
-threads $threads \
-quality good \
-speed 4 \
$subtitleOpts \
-pass 1 \
-an \
-f null /dev/null"

pass2="ffmpeg \
-y \
-i $fileName \
-c:v libvpx-vp9 \
-b:v ${bitrateKb}K \
-crf 30 \
-g 120 \
-tile-columns $tileColumns \
-threads $threads \
-quality good \
-speed 4 \
$subtitleOpts \
-pass 2 \
$audioOpts \
$fileOutput"

echo "$pass1 && $pass2" | bash
